import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as image
import sys
import cv2

"""funcion the umbral con la integral de la imagen"""
def thresholdIntegral(inputMat,img_integral,T = 0.15):
    """
    inputMat: matriz de la imagen en grises
    img_integral : matriz de la imagen integral
    T: porcentaje umbral  
    """
    
    """Crear una matriz de zeros"""
    outputMat=np.zeros(inputMat.shape)
    """Obtener el número de filas y columnas"""
    nRows, nCols = inputMat.shape
    
    """Se calcula el tamaño de la ventana como 1/25 de la máxima dimensión"""
    s2 = int(max(nRows, nCols) * 0.04)

    for i in range(nRows):
        y1 = i - s2
        y2 = i + s2

        if (y1 < 0) :
            y1 = 0
        if (y2 >= nRows):
            y2 = nRows - 1

        for j in range(nCols):
            x1 = j - s2
            x2 = j + s2

            if (x1 < 0) :
                x1 = 0
            if (x2 >= nCols):
                x2 = nCols - 1
            count = (x2 - x1)*(y2 - y1)

            sum=img_integral[y2][x2]-img_integral[y2][x1]-img_integral[y1][x2]+img_integral[y1][x1]

            if ((int)(inputMat[i][j] * count) < (int)(sum*(1.0 - T))):
                outputMat[i][j] = 255

    return outputMat


img_directory="./images/"

images_names={"img_01":"sincholagua.2023.01.01.jpg",
              "img_02":"reventador.2022.12.25.jpeg",
              "img_03":   "merced.2022.12.27.png",
              "img_04":"ruminahui.2022.12.31.jpg"}
img_name="img_03" 
img_path = f"{img_directory}/{images_names[img_name]}"


"""Leer la imagen"""
img = image.imread(img_path)

"""Convertir la imagen a gris y al rango 1 a 255"""
img_gray = np.mean(img*255,axis=2).astype("uint8") 

"""Obtener la imagen integral usando la función INTEGRAL de OPENCV"""               
img_integral = cv2.integral(img_gray)

"""Umbralizar la imagen """
img_umbral = thresholdIntegral(img_gray,img_integral)

"""Plotear la imagen """
plt.imshow(img_umbral,cmap='gray')

plt.show()


